Futurama module README file
-----------------------

What does Futurama do?
---
Futurama is a simple and fun module to display random caption titles from the
animated TV show Futurama.

How does it work?
---
Simple. Just enable the module at Admin > Extend and move it to any region you
want at Admin > Structure > Blocks > Place Blocks. Finally, enjoy it.
