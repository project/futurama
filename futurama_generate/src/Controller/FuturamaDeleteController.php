<?php

namespace Drupal\futurama_generate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class FuturamaDeleteController extends ControllerBase {

  public static function DeleteContent($node) {
    $node->delete();
  }

  public static function BatchFinished() {
    \Drupal::messenger()->addMessage(t('Finished deleting nodes.'));
  }
}
